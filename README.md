# Django todo

A todo list app made with Django 

## Getting Started

    code
$ Git clone https://gitlab.com/officerebel/django_todo.git

$ cd django_todo


### the requirements

```
Make a virtual environment
$virtualenv  django
$ source django/bin/activate

$ Install the requirements
$ pip install -r requirements.txt 

```

### Make super user
```
$ python3 manage.py createsuperuser
```

### Activate the server
```
$ python3 manage.py runserver
```

### Deployment on AWS
```
$ eb init -p python-3.6 django_todo --region us-east-2
$ You must provide your credentials.


```
### Create I Am user group
```
got to
https://console.aws.amazon.com/iam/home#/users
add user
select AWS Management Console access
create a group select AWSElasticBeanstalkFullAccess
copy the keys and past in the CLI

```

### Deploy
cli responds with Application django_todo has been created.

$ eb create django_todo

$Do you wish to continue with CodeCommit? (y/N) (default is n): n

$ eb create  django_todo

$ eb deploy

$ eb open
```

## Built With

* [Django](https://www.djangoproject.com) - The web framework used

## Authors

* **Tim Vogt** - *Initial work* - [Officerebel](https://gitlab.com/officerebel)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

